﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CreateItemDatabase {
    [MenuItem("ItemDatabase/Create Or Find")]
    public static void CreateOrFind() {
        ItemDatabase itemDatabase = Resources.Load<ItemDatabase>("Scripts/ItemDatabase");
        if(!itemDatabase) {
            itemDatabase = ScriptableObject.CreateInstance<ItemDatabase>();
            AssetDatabase.CreateAsset(itemDatabase, "Assets/Resources/Scripts/ItemDatabase.asset");
            AssetDatabase.SaveAssets();
        }

        EditorUtility.FocusProjectWindow();
        Selection.activeObject = itemDatabase;
    }
}