﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using GameData;

public class BuildPlatforms : MonoBehaviour {
    public static string bversion;
    public static string[] paths =  new string[2];
    public static string ext;
    private static BuildTarget[] bps = {BuildTarget.Android, BuildTarget.StandaloneWindows };
    #region MenuItems
    [MenuItem("Build/Android/Dev")]
	public static void BuildAndroidDev() {
		Build(new BuildTarget[] { bps[0] }, "Development", true);
	}

	[MenuItem("Build/Android/Release")]
	public static void BuildAndroidRel() {
		Build(new BuildTarget[] { bps[0] }, "Release", false);
	}

	[MenuItem("Build/Android/All")]
	public static void BuildAndroidAll() {
		Build(new BuildTarget[] { bps[0] }, "All", false);
	}

    [MenuItem("Build/Windows/Dev")]
    public static void BuildWindowsDev() {
        Build(new BuildTarget[] { bps[1] }, "Development", false);
    }

    [MenuItem("Build/Windows/Release")]
    public static void BuildWindowsRel() {
        Build(new BuildTarget[] { bps[1] }, "Release", false);
    }

    [MenuItem("Build/Windows/All")]
    public static void BuildWindowsAll() {
        Build(new BuildTarget[] { bps[1] }, "All", false);
    }

    [MenuItem("Build/All")]
    public static void BuildAll() {
        Debug.Log(DateTime.Now.ToString("yyyyMMddHHmmss"));
        Build(bps, "All", false);
    }
    [MenuItem("Download/Localization")]
    public static void DownloadLocale() {
        Downloader.DownloadCSV(Downloader.LOCALIZATION_TAB_ID, Localization.FilePath);
    }
    [MenuItem("Download/Constants")]
    public static void DownloadConstants() {
        Downloader.DownloadCSV(Downloader.CONSTANTS_TAB_ID, Constants.FilePath);
    }
    [MenuItem("Download/Both")]
    public static void DownloadBoth() {
        Downloader.Init();
    }
#endregion
    public static void Build(BuildTarget[] platform, string buildfor, bool isDev) {
        for (int i = 0; i < platform.Length; i++) {
            switch (platform[i]) {
                case BuildTarget.Android:
                    ext = ".apk";
                    break;
                case BuildTarget.StandaloneWindows:
                    ext = ".exe";
                    break;
            }
            if (buildfor == "All") {
                paths[0] = "Development";
                paths[1] = "Release";
            } else {
                paths[0] = buildfor;
            }
            for (int j = 0; j < paths.Length; j++) {
                if (paths[j] == "Development") {
                    isDev = true;
                }
                string pathToAssets = Application.dataPath;
                string pathToProject = pathToAssets.Replace("/Assets", "");
                // string buildPath = "/Builds/InfiniteRunner/" + platform;
                string buildPath = string.Format("{0}/Builds/{1}/{2}/InfiniteRunner_0_{3}", pathToProject, platform[i], paths[j], DateTime.Now.ToString("yyyyMMddHHmmss") + ext);
                Debug.Log(buildPath);
                BuildPlayerOptions options = new BuildPlayerOptions();
                options.locationPathName = buildPath;
                options.options = isDev ? BuildOptions.Development : BuildOptions.None;
                options.target = platform[i];
                options.scenes = new string[] { "Assets/" + platform[i] + "/Scenes/Start.unity" };
                BuildPipeline.BuildPlayer(options);
                Debug.Log("Building " + platform + " " + buildfor);
            }
        }

	}
}