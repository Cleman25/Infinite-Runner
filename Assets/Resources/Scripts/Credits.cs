﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Credits : MonoBehaviour {
    public Button mainMenu;

	void Start () {
        SoundManager.instance.playMusic(SoundManager.instance.startMusic);
        mainMenu.onClick.AddListener(() => GameManager.instance.NextScene("start"));
    }
}
