﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour {
    static SoundManager _instance = null;
    private const string SFX_VOL = "sfxvol";
    private const string MUSIC_VOL = "musicvol";
    private const string MASTER_VOL = "mastervol";
    // [Header("Audio Sources")]

    [Header("Audio Mixers")]
    [SerializeField]
    private AudioMixer _masterMixer;

    [Header("Volumne Sliders")]
    [SerializeField]
    private Slider musicVolumeSlider;
    [SerializeField]
    private Slider sfxVolumeSlider;
    [SerializeField]
    private Slider masterVolumeSlider;

    [Header("AudioSources")]
    public AudioSource musicSource;
    public AudioSource soundSource;

    [Header("Music Clips")]
    public AudioClip menuMusic;
    public AudioClip gameMusic;
    public AudioClip startMusic;
    public AudioClip creditsMusic;
    public AudioClip gameLost;

    [Header("SFx Clips")]
    public AudioClip jumpSound;
    public AudioClip dieSound;
    public AudioClip shootSound;
    public AudioClip coinSound;

    void Awake () {
        if(instance) {
            DestroyImmediate(gameObject);
        } else {
            instance = this;
            DontDestroyOnLoad(this);
        }
    }
    // Use this for initialization
    void Start() {
        SetFromSaveFile();
    }


    public void masterSliderChange() {
        SetMasterVolume(masterVolumeSlider.value);
    }

    public void sfxSliderChange() {
        SetSFxVolume(sfxVolumeSlider.value);
    }

    public void musicSliderChange() {
        SetMusicVolume(musicVolumeSlider.value);
    }

    public void SetSFxVolume(float pSFxVol) {
        _masterMixer.SetFloat(SFX_VOL, pSFxVol);
    }

    public void SetMusicVolume(float pMusicVol) {
        _masterMixer.SetFloat(MUSIC_VOL, pMusicVol);
    }

    public void SetMasterVolume(float pMasterVol) {
        _masterMixer.SetFloat(MASTER_VOL, pMasterVol);
    }

    public void Save() {
        float mvol = 0f;
        _masterMixer.GetFloat(MUSIC_VOL, out mvol);
        PlayerPrefs.SetFloat(MUSIC_VOL, mvol);
        float svol = 0f;
        _masterMixer.GetFloat(SFX_VOL, out svol);
        PlayerPrefs.SetFloat(SFX_VOL, svol);
        float msvol = 0f;
        _masterMixer.GetFloat(MASTER_VOL, out msvol);
        PlayerPrefs.SetFloat(MASTER_VOL, msvol);
    }

    public void SetFromSaveFile() {
        musicVolumeSlider.value = PlayerPrefs.GetFloat(MUSIC_VOL, 0);
        sfxVolumeSlider.value = PlayerPrefs.GetFloat(SFX_VOL, 0);
        masterVolumeSlider.value = PlayerPrefs.GetFloat(MASTER_VOL, 0);
    }

    public void OnClearData() {
        musicVolumeSlider.value = 0f;
        sfxVolumeSlider.value = 0f;
        masterVolumeSlider.value = 0f;
    }

    public static SoundManager instance {
        get { return _instance; }
        set { _instance = value; }
    }

    public void ToggleMasterVol() {
        masterVolumeSlider.value = masterVolumeSlider.minValue;
    }

    public void ToggleMusciVol() {
        musicVolumeSlider.value = musicVolumeSlider.minValue;
    }

    public void ToggleSFxVol() {
        sfxVolumeSlider.value = sfxVolumeSlider.minValue;
    }

    public void playMusic(AudioClip musicClip) {
        musicSource.clip = musicClip;
        musicSource.volume = 1f;
        musicSource.Play();
    }

    public void playSound(AudioClip soundClip) {
        soundSource.clip = soundClip;
        soundSource.volume = 1f;
        soundSource.Play();
    }
}
