﻿using UnityEngine;
using System.Collections;

public class PauseManager : Singleton<PauseManager> {

	public delegate void OnPause();
	public delegate void OnResume();

	private OnPause onPause;
	private OnResume onResume;

	[HideInInspector] public bool isPaused = false;
		
	public void AddToPause(IPausable p){
		onPause += p.OnPause;
		onResume += p.OnResume;
	}

	public void RemoveFromPause(IPausable p){
		onPause -= p.OnPause;
		onPause -= p.OnResume;
	}

	public void Pause(){
		if (onPause != null) {
			onPause ();
        }
		isPaused = true;
	}

	public void Resume(){
		if (onResume != null) {
			onResume ();
        }
		isPaused = false;
	}

	public void TogglePause() {
		if (isPaused) {
            Resume();
        } else {
			Pause ();
        }
    }
}