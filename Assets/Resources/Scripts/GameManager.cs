﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using GameData;

public class GameManager : MonoBehaviour {

    static GameManager _instance;
    public GameObject playerPrefab;
    public Button startBtn;
    public Button quitBtn;
    public Button exitBtn;
    public Button instButton;
    public Button creditsButton;
    public Button menuButton;
    public Button infoButton;
    public Text livesText;

    [SerializeField]
    private int _lives;

    public Canvas hud;
    public Canvas inst;
    public Canvas menu;
    public Canvas start;
    public bool gameLost;
    public bool gameIsOver;
    
    void Awake () {
        if(instance) {
            DestroyImmediate(gameObject);
        } else {
            instance = this;
            DontDestroyOnLoad(this);
        }
        Downloader.Init();
        Constants.Initialize();
        Localization.Initialize();
    }

    void Start () {
        Time.timeScale = 1;
        if (!hud) {
            Debug.Log("Could not find hud");
            hud = GameObject.FindGameObjectWithTag("Hud").GetComponent<Canvas>();
        }
        if (!inst) {
            Debug.Log("Could not find inst");
            inst = GameObject.FindGameObjectWithTag("Inst").GetComponent<Canvas>();
        }
        if (!menu) {
            Debug.Log("Could not find menu");
            menu = GameObject.FindGameObjectWithTag("Menu").GetComponent<Canvas>();
        }
        if(!start) {
            Debug.Log("Start not found");
            start = GameObject.FindGameObjectWithTag("StartCanvas").GetComponent<Canvas>();
        }

        creditsButton.onClick.AddListener(() => {
            NextScene(scene: "credits");
            SoundManager.instance.playMusic(SoundManager.instance.creditsMusic);
        });
        instButton.onClick.AddListener(() => loadCanvas(inst));
        infoButton.onClick.AddListener(() => loadCanvas(inst));
        menuButton.onClick.AddListener(() => loadCanvas(menu));
        exitBtn.onClick.AddListener(() => {
            NextScene(scene: "start");
            SoundManager.instance.playMusic(SoundManager.instance.startMusic);
        });
        lives = Constants.Get<int>("MAX_LIVES");
        SoundManager.instance.playMusic(SoundManager.instance.startMusic);
    }
    
    void Update () {
        if (Input.GetKeyDown(KeyCode.Return)) {
            if (SceneManager.GetActiveScene().name == "Start") {
                SoundManager.instance.playMusic(SoundManager.instance.gameMusic);
                SceneManager.LoadScene("Level");
            }
        } else if (Input.GetKeyDown(KeyCode.Escape)) {
            if (SceneManager.GetActiveScene().name == "Level") {
                SoundManager.instance.playMusic(SoundManager.instance.startMusic);
                SceneManager.LoadScene("start");
            } else if (SceneManager.GetActiveScene().name == "Credits") {
                SoundManager.instance.playMusic(SoundManager.instance.startMusic);
                SceneManager.LoadScene("start");
            }
        }

        if (Input.GetKeyDown(KeyCode.I)) {
            if (inst.gameObject.activeSelf) {
                inst.gameObject.SetActive(false);
            } else {
                inst.gameObject.SetActive(true);
            }
        }
        if (Input.GetKeyDown(KeyCode.M)) {
            if (SceneManager.GetActiveScene().name == "Level") {
                if (menu.gameObject.activeSelf) {
                    Time.timeScale = 1;
                    menu.gameObject.SetActive(false);
                } else {
                    Time.timeScale = 0;
                    menu.gameObject.SetActive(true);
                }
            }
        }

        if(SceneManager.GetActiveScene().name == "Level") {
            hud.gameObject.SetActive(true);
        } else {
            hud.gameObject.SetActive(false);
        }
        if (gameLost) {
            gameIsOver = true;
        }

        if(SceneManager.GetActiveScene().name == "Start" && start.gameObject.activeSelf == false) {
            start.gameObject.SetActive(true);
        } else {
            start.gameObject.SetActive(false);
        }

        if(gameIsOver) {
            EndGame();
        }
	}

    public void NextScene(string scene) {
        SceneManager.LoadScene(scene);
    }

    public bool checkScene(string scene) {
        if (SceneManager.GetSceneByName(scene).IsValid()) {
            return true;
        } else {
            return false;
        }
    }

    public void QuitGame() {
        Debug.Log("Quitting Game...");
        Application.Quit();
    }

    public void EndGame() {
        SoundManager.instance.playMusic(SoundManager.instance.startMusic);
        NextScene("Start");
        ScoreManager.instance.Save();
        SoundManager.instance.Save();
    }

    public void StartGame() {
        SoundManager.instance.playMusic(SoundManager.instance.gameMusic);
        NextScene("Level");
        SoundManager.instance.SetFromSaveFile();
    }
    
    public static GameManager instance {
        get { return _instance; }
        set { _instance = value; }
    }

    public int lives {
        get { return _lives; }
        set {
            _lives = value;
            livesText.text = "x" + value;
        }
    }

    public void loadCanvas(Canvas c) {
        if (c.gameObject.activeSelf == true) {
            if(c == hud) {
                Time.timeScale = 0;
            }
            Debug.Log(c.name + " is already open.");
        } else {
            c.gameObject.SetActive(true);
            if(c == hud) {
                Time.timeScale = 1;
            }
        }
    }
}
