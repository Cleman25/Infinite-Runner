﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    public Character player;
    public Vector3 lastPlayerPosition;
    public float distanceToMove;
	void Start () {
        player = FindObjectOfType<Character>();
        lastPlayerPosition = player.transform.position;
    }

    void Update () {
        if (player) {
            distanceToMove = player.transform.position.x - lastPlayerPosition.x;

            transform.position = new Vector3(transform.position.x + distanceToMove, transform.position.y, transform.position.z);

            lastPlayerPosition = player.transform.position;
        }
    }

    public void GoToPlayer() {
        transform.position = new Vector3(player.transform.position.x, transform.position.y, transform.position.z);
    }
}
