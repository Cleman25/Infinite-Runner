﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData;

public class DeathBlock : MonoBehaviour {
    public RestartManager rm;
    void Start() {
        Constants.Initialize();
        rm = FindObjectOfType<RestartManager>();
    }
    void OnTriggerEnter2D (Collider2D c) {
        if (c.gameObject.CompareTag("Player")) {
            SoundManager.instance.playSound(SoundManager.instance.dieSound);
            Character p = c.gameObject.GetComponent<Character>();
            GameManager.instance.lives -= 1;
            if (GameManager.instance.lives <= 0) {
                ScoreManager.instance.increaseScore = false;
                GameManager.instance.lives = 0;
                GameManager.instance.gameLost = true;
                Destroy(c.gameObject);
            } else {
                rm.RestartGame();
                p.speed = p.speedStore;
                p.speedMilestone = p.speedMStore;
                p.speedMilestoneCount = p.speedMCStore;
                ScoreManager.instance.OnSubtractPoints(Constants.Get<int>("SUBTRACT_ON_DEATH"));
                if (ScoreManager.instance.currentScore <= 0) {
                    ScoreManager.instance.currentScore = 0;
                }
            }
        }
    }

    void OnCollisionEnter2D(Collision2D c) {
        if (c.gameObject.CompareTag("Player")) {
            Character p = c.gameObject.GetComponent<Character>();
            GameManager.instance.lives -= 1;
            if (GameManager.instance.lives <= 0) {
                ScoreManager.instance.increaseScore = false;
                GameManager.instance.lives = 0;
                GameManager.instance.gameLost = true;
                Destroy(c.gameObject);
            } else {
                rm.RestartGame();
                p.speed = p.speedStore;
                p.speedMilestone = p.speedMStore;
                p.speedMilestoneCount = p.speedMCStore;
                ScoreManager.instance.OnSubtractPoints(Constants.Get<int>("SUBTRACT_ON_DEATH"));
                if (ScoreManager.instance.currentScore <= 0) {
                    ScoreManager.instance.currentScore = 0;
                }
            }
        }
    }
}
