﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizationManager : MonoBehaviour {
    public Text item;
    public string key; // item

    void Start () {
        Localization.Initialize();
        if(!item) {
            item = GetComponent<Text>();
        }
        SetText(key);
    }

    void Update() {
        SetText(key);
    }

    void SetText(string k) {
        if(k == string.Empty) {
            Debug.Log("Key cannot be empty");
        } else {
            if (item.text == string.Empty || item.text == "New Text") {
                string translation = Localization.Get(k);
                item.text = translation;
                Debug.Log("Value set to: " + item.text);
            }
        }
    }
}