﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData;
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Animator))]
//[RequireComponent(typeof(CharacterController))]
public class Character : MonoBehaviour {

    [SerializeField] private bool onGround;
    public Transform groundCheck;
    [SerializeField] private float groundCheckRadius = 0.2f;
    public LayerMask groundLayer;

    [Header("Speed Settings")]
    public float speed;
    public float speedBoost = 0f;
    public float speedMultiplier;
    public float speedMilestone;
    public float speedMilestoneCount;
    public float speedStore;
    public float speedMCStore;
    public float speedMStore;

    [Header("Components")]
    [SerializeField] private Rigidbody2D rb;
    //public BoxCollider2D bc;
    public Animator anim;

    public Vector3 rayStart;
    public Vector3 startPos;

    public Vector3 mouseStartPos;
    public static float MIN_SWIPE_LENGTH = 50f;
    public static float MAX_SWIPE_TIME = 10;
    public float elapsedTime;
    public bool startTimer;
    public bool enableFreeSwipe = true;

    [Header("Touch Settings")]
    private Vector3 fp;   //First touch position
    private Vector3 lp;   //Last touch position
    private float dragDistance;  //minimum distance for a swipe to be registered
    private List<Vector3> touchPositions = new List<Vector3>(); //store all the touch positions in list

    [Header("Jump Settings")]
    public float jumpForce;
    public float jumpTime;
    public float jumpTimeCounter;
    public bool isJumping;
    public bool canDoubleJump;

    public Effector eff;

    public GameObject tapObject;
    public Transform tapObjectPoint;
    public bool tapped;
    int i;
	void Start () {
        if (jumpForce <= 0) {
            jumpForce = 10f;
        }
        if (speed <= 0) {
            speed = 2f;
        }
        mouseStartPos = Vector3.zero;
        elapsedTime = 0;
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        startPos = transform.position;
        jumpTimeCounter = jumpTime;
        speedMilestoneCount = speedMilestone;
        speedStore = speed;
        speedMCStore = speedMilestoneCount;
        speedMStore = speedMilestone;
        isJumping = false;
        i = 0;
    }
	
	void Update () {
        /*#if UNITY_EDITOR
                SimulateMouseSwipe();
        #endif*/
        onGround = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, groundLayer);
        if (transform.position.x > speedMilestoneCount) {
            speedMilestoneCount += speedMilestone;
            speedMilestone += speedMilestone * speedMultiplier;
            speed = speed * speedMultiplier;
            if(speedMilestoneCount > 50 && false && tapped == true) {
                GameObject tapObject = Instantiate(tapObject, tapObjectPoint.position, tapObjectPoint.rotation) as GameObject;
                tapped = false;
            }
        }
        rb.velocity = new Vector2(speed, rb.velocity.y);

        if(speedMilestoneCount == 100) {
            eff.toggle = true;
        } else {
            eff.toggle = false;
        }

        if ((Input.GetKeyDown(KeyCode.Space))) {
            SoundManager.instance.playSound(SoundManager.instance.jumpSound);
            if (onGround) {
                rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                isJumping = false;
                ScoreManager.instance.OnAddPoints(Constants.Get<int>("ADD_POINTS"));
            } else if (!onGround && canDoubleJump) {
                rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                jumpTimeCounter = jumpTime;
                isJumping = true;
                canDoubleJump = false;
            }
        }
        if ((Input.GetKey(KeyCode.Space) || Input.GetMouseButton(0)) && isJumping) {
            SoundManager.instance.playSound(SoundManager.instance.jumpSound);
            if (jumpTimeCounter > 0) {
                rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                jumpTimeCounter -= Time.deltaTime;
            }
        }

        if (Input.GetKeyUp(KeyCode.Space) || Input.GetMouseButtonUp(0)) {
            jumpTimeCounter = 0;
            isJumping = false;
        }

        if (onGround) {
            jumpTimeCounter = jumpTime;
            canDoubleJump = true;
        }
        foreach (Touch touch in Input.touches) {
            if (touch.phase == TouchPhase.Moved) {
            touchPositions.Add(touch.position);
            }
 
            if (touch.phase == TouchPhase.Ended) {
                //lp = touch.position;  //last touch position. Ommitted if you use list
                fp =  touchPositions[0]; //get first touch position from the list of touches
                lp =  touchPositions[touchPositions.Count-1]; //last touch position 
 
                //Check if drag distance is greater than 20% of the screen height
                if (Mathf.Abs(lp.x - fp.x) > dragDistance || Mathf.Abs(lp.y - fp.y) > dragDistance) { 
                    if (Mathf.Abs(lp.x - fp.x) > Mathf.Abs(lp.y - fp.y)) {
                        if ((lp.x>fp.x)) {
                            Debug.Log("Right Swipe");
                        } else {
                            Debug.Log("Left Swipe"); 
                        }
                    } else {
                            if (lp.y>fp.y) {
                                Debug.Log("Up Swipe");
                                if (onGround) {
                                    rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                                ScoreManager.instance.OnAddPoints(Constants.Get<int>("ADD_POINTS"));
                                }
                            } else {
                                anim.SetTrigger("Duck");
                                Debug.Log("Down Swipe");
                            }
                    }
                } 
            } else {   //It's a tap as the drag distance is less than 20% of the screen height
                Ray ray = Camera.main.ScreenPointToRay(touch.position);
                RaycastHit2D hit = Physics2D.Raycast(ray.origin, -Vector2.up);
                Debug.Log("Tapped!");
                if (hit.collider != null) {
                    Debug.Log("Hit: " + hit);
                    if(hit.collider.CompareTag("tapObject")) {
                        ScoreManager.instance.OnAddPoints(500);
                        Destroy(tapObject);
                        tapped = true;
                    }
                } else {
                    Debug.Log("You hit nothing");
                }
            }
        }
        if (startTimer) {
			if (elapsedTime < MAX_SWIPE_TIME) {
				elapsedTime += Time.deltaTime;
			}

			if (elapsedTime >= MAX_SWIPE_TIME) {
				startTimer = false;
			}
		}
        anim.SetFloat("Speed", rb.velocity.x);
        anim.SetBool("OnGround", onGround);
    }

    public void SpeedBoost() {
        rb.velocity = new Vector2(rb.velocity.x * 3, rb.velocity.y);
    }

    private void SimulateMouseSwipe() {
		if (Input.GetMouseButtonDown(0)) {
			mouseStartPos = Input.mousePosition;
			startTimer = true;
			elapsedTime = 0f;
		}

		if (Input.GetMouseButtonUp(0) && elapsedTime < MAX_SWIPE_TIME) {
			startTimer = false;

			Vector3 mouseEndPos = Input.mousePosition;
			Vector3 direction = (mouseEndPos -mouseStartPos);

			
            if (onGround) {
                rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                ScoreManager.instance.OnAddPoints(Constants.Get<int>("ADD_POINTS"));
            }
		}
	}

    void OnCollisionEnter2D(Collision2D c) {
        if (c.gameObject.CompareTag("Enemy")) {
            rb.transform.position = new Vector2(-2, 2);
        }
    }

    void PlayAnimation(string animation) {
        anim.Play(animation);
    }
}
