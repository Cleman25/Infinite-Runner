﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasManager : MonoBehaviour {
    public Canvas canvas;
    public Button goBack;
    public bool pauseGame = false;
    // Use this for initialization
    void Start () {
        canvas = GetComponent<Canvas>();
        if (goBack) {
            goBack.onClick.AddListener(Back);
        }
    }
	
	// Update is called once per frame
	void Update () {
        if(pauseGame) {
            if (canvas.gameObject.activeSelf == true) {
                Time.timeScale = 0;
            } else {
                Time.timeScale = 1;
            }
        }
    }

    public void Back() {
        canvas.gameObject.SetActive(false);
    }

    public void clickToPause() {
        PauseManager.Instance.TogglePause();
        ToggleMe();
    }

    public void ToggleMe() {
        if (gameObject.activeSelf) {
            gameObject.SetActive(false);
        } else {
            gameObject.SetActive(true);
        }
    }

    public void loadInst() {
        GameManager.instance.loadCanvas(GameManager.instance.inst);
    }
}