﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {
    private static ScoreManager _instance = null;
    public float pointsPerSecond;
    public bool increaseScore;
    public const string HIGHCORE_KEY = "Highscore";
    [Header("Highscore variable")]
    public Text highScoreText;
    public Text currentScoreText;
    [SerializeField]
    private int _highScore;
    [SerializeField]
    private int _currentScore;

    void Awake () {
        if(instance) {
            DestroyImmediate(gameObject);
        } else {
            instance = this;
            DontDestroyOnLoad(this);
        }
    }
    // Use this for initialization
    void Start() {
        currentScore = 0;
        increaseScore = true;
        if(pointsPerSecond <= 0) {
            pointsPerSecond = 5f;
        }
        highScore = PlayerPrefs.GetInt(HIGHCORE_KEY, 0);
        LoadSaveFile();
        // DisplaySaveFile();
    }

    // Update is called once per frame
    void Update() {
        if (increaseScore) {
            currentScore += (int) (pointsPerSecond * Time.deltaTime);
            if (currentScore > highScore) {
                highScore = currentScore;
                Save();
            }
        }
    }

    public void OnAddPoints(int pPoints) {
        currentScore += pPoints;
        Save();
    }

    public void OnSubtractPoints(int pPoints) {
        currentScore = (currentScore - pPoints < 0) ? 0 : currentScore - pPoints;
        Save();
    }

    public void OnSaveData() {
        if (currentScore > highScore) {
            highScore = currentScore;
            Save();
        }
        DisplaySaveFile();
    }

    public int currentScore {
        get { return _currentScore; }
        set {
            _currentScore = value;
            currentScoreText.text = currentScore.ToString();
        }
    }

    public void OnClearData() {
        PlayerPrefs.DeleteAll();
        highScore = 0;
        DisplaySaveFile();
    }

    public void LoadSaveFile() {
        highScore = PlayerPrefs.GetInt(HIGHCORE_KEY, 0);
    }

    public void Save() {
        PlayerPrefs.SetInt(HIGHCORE_KEY, highScore);
    }

    public int highScore {
        get { return _highScore; }
        set {
            _highScore = value;
            highScoreText.text = highScore.ToString();
        }
    }

    public void DisplaySaveFile() {
        highScoreText.text = string.Format("{0}", highScore);
    }

    public static ScoreManager instance {
        get { return _instance; }
        set { _instance = value; }
    }
}
