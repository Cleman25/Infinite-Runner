﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace GameData {
	public static class Constants {
		private static Dictionary<string,object> vals = new Dictionary<string, object>();

        public static string FilePath {
            get {
                string persistentPath = Application.persistentDataPath;
                string savePath = persistentPath + "/" + "Constants.csv";
                    return savePath;
            }
        }

	    public static void Initialize() {
            vals.Clear();
            StreamReader sr = File.OpenText(FilePath);
            string[] rows = sr.ReadToEnd().Split('\n');
            Debug.Log(rows);

			for(int i = 1; i < rows.Length;i++) {
				string[] cols = rows[i].Split(',');
				string key = cols[0];
				string type = cols[1];
				string value = cols[2];
			}
			foreach(string l in rows) {
				string[] data = l.Split(',');

				if(data.Length == 3) {
					string key = data[0];
					string type = data[1];
					string value = data[2];

					if(type == "INT") {
						int v;
						if(int.TryParse(value,out v)) {
							vals.Add(key,v);
						} else {
							Debug.Log("Not an int.");
						}
					} else if(type == "FLOAT") {
						float f;
						if(float.TryParse(value, out f)) {
							vals.Add(key,f);
						} else {
							Debug.Log("Not a float.");
						}
					} else {
						vals.Add(key, value);
					}
				}
            }
            sr.Close();
        }

        public static T Get<T>(string key) {
			return (T)vals[key];
		}
	}
}

