﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace GameData {
	public static class Downloader {
		//Get these from your own URL
		const string SPREADSHEET_ID = "1toF4XtV1RrewIgiqtKCnyId3aO-VuJ_ZOfad2U7grfM"; //after the d/
		public const string LOCALIZATION_TAB_ID = "1663300165"; //gid
        public const string CONSTANTS_TAB_ID = "0";
		const string URL_FORMAT = "https://docs.google.com/spreadsheets/d/{0}/export?format=csv&id={0}&gid={1}";

		//Get url by using File->Download As->CSV
		//Then open download history in chrome/firefox and copy url
		//Make sure accessiblity is set to Anyone with link can view

		public static void Init() {
			DownloadCSV(LOCALIZATION_TAB_ID, Localization.FilePath);
			DownloadCSV(CONSTANTS_TAB_ID, Constants.FilePath);
		}

		public static void DownloadCSV(string tabID, string filePath) {

			//Get the formatted URL
			string downloadUrl = string.Format(URL_FORMAT, SPREADSHEET_ID, tabID);
			Debug.LogFormat("Downloading {0}", tabID);
            Debug.Log(downloadUrl);
			//Download the data
			WWW website = new WWW(downloadUrl);
            Debug.Log(website);
			//Wait for data to download
			while (!website.isDone) {
			}
            if(website.isDone) {
			    if (string.IsNullOrEmpty(website.text)) {
				    Debug.LogError("NO DATA WAS RECEIVED");
                    //Load the last cached values
                    if (File.Exists(filePath)) {
                        File.ReadAllText(filePath);
                    }
			    } else {
                    //Successfully got the data, process it
                    //Save to disk
                    File.WriteAllText(filePath, website.text);
			    }
            }
		}
	}
}
