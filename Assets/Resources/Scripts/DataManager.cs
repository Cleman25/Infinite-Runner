﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataManager : MonoBehaviour {
	public const string HIGHCORE_KEY = "Highscore";
	[Header("Highscore variable")]
    [SerializeField]
	private Text _highScoreText;
    [SerializeField]
    private Text _currentScoreText;
    [SerializeField]
    private int _highScore;
    [SerializeField]
    private int _currentScore;

	void Awake() {
		_currentScore = 0;
		_currentScoreText.text = string.Format("Current Score: {0}", _currentScore);
	}

	void Start() {
		LoadSaveFile();
		DisplaySaveFile();
	}

	public void OnAddPoints(int pPoints) {
        pPoints = 10;
		_currentScore += pPoints;
		_currentScoreText.text = string.Format("Current Score: {0}", _currentScore);
	}

	public void OnSubtractPoints(int pPoints) {
        pPoints = 10;
		_currentScore = (_currentScore - pPoints < 0) ? 0 : _currentScore - pPoints;
		_currentScoreText.text = string.Format("Current Score: {0}", _currentScore);
	}

    public void OnClearData() {
        PlayerPrefs.DeleteAll();
        _highScore = 0;
        DisplaySaveFile();
    }


    public void OnSaveData() {
		if(_currentScore > _highScore) {
			_highScore = _currentScore;
			Save();
		}
        DisplaySaveFile();
    }

	private void LoadSaveFile() {
		_highScore = PlayerPrefs.GetInt(HIGHCORE_KEY, 0);
	}

	private void DisplaySaveFile() {
		_highScoreText.text = string.Format("Highscore: {0}", _highScore);
	}

	public void Save() {
		PlayerPrefs.SetInt(HIGHCORE_KEY, _highScore);
	}
}
