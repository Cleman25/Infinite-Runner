﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGenerator : MonoBehaviour {
    public GameObject platform;
    public Transform generationPoint;
    public float distanceBetween;
    public float platformWidth;

    public float distanceBetweenMin;
    public float distanceBetweenMax;

    //public GameObject[] platforms;
    public float[] platformWidths;
    public int platformSelector;

    public ObjectPooler[] objectPooler;

    [SerializeField] private float minHeight;
    public Transform maxHeightPoint;
    [SerializeField] private float maxHeight;
    public float maxHeightChange;
    [SerializeField] private float heightChange;

    [SerializeField] private CoinGenerator coinGen;
    public float coinThreshold = 75f;

	// Use this for initialization
	void Start () {
        //platformWidth = platform.GetComponent<BoxCollider2D>().size.x;
        platformWidths = new float[objectPooler.Length];
        for (int i = 0; i < objectPooler.Length; i++) {
            platformWidths[i] = objectPooler[i].pooledObject.GetComponent<BoxCollider2D>().size.x;
        }
        //objectPooler = GameObject.Find("ObjectPooler").GetComponent<ObjectPooler>();
        minHeight = transform.position.y;
        maxHeight = maxHeightPoint.position.y;
        coinGen = FindObjectOfType<CoinGenerator>();
    }
	
	// Update is called once per frame
	void Update () {
		if (transform.position.x < generationPoint.position.x) {

            distanceBetween = Random.Range(distanceBetweenMin, distanceBetweenMax);
            platformSelector = Random.Range(0, objectPooler.Length);
            heightChange = transform.position.y + Random.Range(maxHeightChange, -maxHeightChange);
            if (heightChange > maxHeight) {
                heightChange = maxHeight;
            } else if (heightChange < minHeight) {
                heightChange = minHeight;
            }
            transform.position = new Vector3(transform.position.x + (platformWidths[platformSelector] / 2) + distanceBetween, heightChange, transform.position.x);
            

            //Instantiate(/*platform*/ platforms[platformSelector], transform.position, transform.rotation);

            GameObject newPlatform = objectPooler[platformSelector].GetPooledObjects();
            newPlatform.transform.position = transform.position;
            newPlatform.transform.rotation = transform.rotation;
            newPlatform.SetActive(true);

            if(Random.Range(0f, 100f) < coinThreshold) {
                coinGen.SpawnCoins(new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z));
            }

            transform.position = new Vector3(transform.position.x + (platformWidths[platformSelector] / 2), transform.position.y, transform.position.x);
        }

    }
}
