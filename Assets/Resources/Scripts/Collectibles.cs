﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectibles : MonoBehaviour {
    public int worth;
    void Start() {
    }
    void OnTriggerEnter2D (Collider2D c) {
        if(c.gameObject.CompareTag("Player")) {
            ScoreManager.instance.OnAddPoints(worth);
            SoundManager.instance.playSound(SoundManager.instance.coinSound);
            gameObject.SetActive(false);
            // Destroy(gameObject);
        }
    }
}
