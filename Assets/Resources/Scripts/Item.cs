﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Item {
    public string name;
    public float weight;
    public Rarity rarity;
    public int value;
    public Sprite Icon;
}

public enum Rarity {
    Common,
    Uncommon,
    Rare,
    Epic,
    UberMax,
    ChuckNorris
}