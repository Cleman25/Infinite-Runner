﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartManager : MonoBehaviour {

    public Transform platformGenerator;
    public Vector3 platformStartPoint; // private

    public Character character; // playerPrefab; private
    public Vector3 playerStartPoint; // private

    public PlatformDestroyer[] platformList;

    void Start() {
        platformStartPoint = platformGenerator.position;
        playerStartPoint = character.transform.position;
    }

    public void RestartGame() {
        StartCoroutine("RestartGameCo");
    }

    public IEnumerator RestartGameCo() {
        character.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        platformList = FindObjectsOfType<PlatformDestroyer>();
        for (int i = 0; i < platformList.Length; i++) {
            platformList[i].gameObject.SetActive(false);
        }
        character.transform.position = playerStartPoint;
        platformGenerator.position = platformStartPoint;
        character.gameObject.SetActive(true);
    }
}

