﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class Localization {
    //The dictionary
    static SystemLanguage locale;
	static Dictionary<string, object> localizations = new Dictionary<string, object>();

    public static string FilePath {
        get {
            string persistentPath = Application.persistentDataPath;
            string savePath = persistentPath + "/" + "Localizations.csv";
                return savePath;
        }
    }

	public static void Initialize() {
        localizations.Clear();
        StreamReader sr = File.OpenText(FilePath);
        string[] rows = sr.ReadToEnd().Split('\n');
        Debug.Log(rows);
        //Get the system language
        locale = Application.systemLanguage;
		//Default language to english
		int languageIndex = 1;

		for (int i = 1; i < rows.Length; i++) {
            //Get the language index
            if (locale == SystemLanguage.English) {
                languageIndex = 1;
            } else if (locale == SystemLanguage.Spanish) {
                languageIndex = 2;
            } else if(locale == SystemLanguage.French) {
                languageIndex = 3;
            }
            string[] cols = rows[i].Split(',');
            string identifier = cols[0];
            string english = cols[1];
            string spanish = cols[2];
            string french = cols[3];
		}
        foreach(string l in rows) {
            string[] data = l.Split(',');

            if(data.Length == 4) {
                string identifier = data[0];
                string english = data[1];
                string spanish = data[2];
                string french = data[3];

                localizations.Add(identifier, data[languageIndex]);
            }
        }
        sr.Close();
    }

	//Get the localized string
	public static string Get(string id) {
        //Make sure the CSV has been loaded
        if (localizations[id] == null) {
            return "Failed to load csv";
        }

        return (string)localizations[id];
	}
}
