﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinGenerator : MonoBehaviour {
    public ObjectPooler coinPool;
    public float coinDistance;
    
	public void SpawnCoins(Vector3 startPos) {
        GameObject coin1 = coinPool.GetPooledObjects();
        coin1.transform.position = startPos;
        coin1.SetActive(true);

        GameObject coin2 = coinPool.GetPooledObjects();
        coin2.transform.position = new Vector3(startPos.x - coinDistance, startPos.y, startPos.z);
        coin2.SetActive(true);

        GameObject coin3 = coinPool.GetPooledObjects();
        coin3.transform.position = new Vector3(startPos.x + coinDistance, startPos.y, startPos.z);
        coin3.SetActive(true);
    }
}
