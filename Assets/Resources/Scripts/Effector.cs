﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(BoxCollider2D))]
public class Effector : MonoBehaviour {
    
    public enum Effects {
        Floater,
        SlowDown,
        SpeedUp,
        Swim
    }
    public Effects effect;
    public Character player;
    public bool toggle;
    public float playerSpeed;

	void Start () {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
	}

    void FixedUpdate() {
        if(toggle) {
            SlowTime();
        } else {
            SpeedUp();
        }
         
    }

    public void SlowTime() {
        Time.timeScale = 0.5f;
        Vector3 lerpTo = new Vector3(player.transform.position.x, 1.5f, player.transform.position.y);
        player.transform.position = Vector3.Lerp(player.transform.position, lerpTo, Time.fixedDeltaTime);
        //player.speed *= 2;
    }

    public void SpeedUp() {
        Time.timeScale = 1f;
        //player.speed = playerSpeed;
    }
}
