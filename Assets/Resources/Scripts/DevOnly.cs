﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DevOnly : MonoBehaviour {
	void Awake() {
		gameObject.SetActive(Debug.isDebugBuild);
	}
}